﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.UI.Model
{
    internal class EditorModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ListEditorModel _mother;

        private PropertyRow _row;

        private string _name;

        private Helper.EventTriggers _trigger;

        private int _updateSkip;

        private bool _parallel;

        private bool _isEditing;

        private bool _isImporting;

        private Helper.OutputType _outputType;

        private ExportRow _exportRow;

        public ListEditorModel Mother
        {
            get => _mother;
            set
            {
                _mother = value;
                OnPropertyChanged();
            }
        }

        public PropertyRow Row
        {
            get => _row;
            set
            {
                _row = value;
                OnPropertyChanged();

                Name = value.Name;
            }
        }

        public string Name
        {
            get => _name;

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public Helper.EventTriggers Trigger
        {
            get => _trigger;

            set
            {
                _trigger = value;
                OnPropertyChanged();
            }
        }

        public Helper.OutputType OutputType
        {
            get => _outputType;

            set
            {
                _outputType = value;
                OnPropertyChanged();
            }
        }

        public Helper.EventTriggers[] GetAllTriggers
        {
            get
            {
                Array enumVal = typeof(Helper.EventTriggers).GetEnumValues();

                Helper.EventTriggers[] arr = new Helper.EventTriggers[enumVal.Length];
                for (int i = 0; i < enumVal.Length; i++)
                {
                    arr[i] = (Helper.EventTriggers)enumVal.GetValue(i);
                }

                return arr;
            }
        }

        public Helper.OutputType[] GetAllOutputTypes
        {
            get
            {
                Array enumVal = typeof(Helper.OutputType).GetEnumValues();

                Helper.OutputType[] arr = new Helper.OutputType[enumVal.Length];
                for (int i = 0; i < enumVal.Length; i++)
                {
                    arr[i] = (Helper.OutputType)enumVal.GetValue(i);
                }

                return arr;
            }
        }

        public int UpdateSkip
        {
            get => _updateSkip;

            set
            {
                if (value < 0)
                    value = 0;

                _updateSkip = value;
                OnPropertyChanged();
            }
        }

        public bool Parallel
        {
            get => _parallel;

            set
            {
                _parallel = value;
                OnPropertyChanged();
            }
        }

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                _isEditing = value;
                OnPropertyChanged();
            }
        }

        public bool IsImporting
        {
            get => _isImporting;
            set
            {
                _isImporting = value;
                OnPropertyChanged();
            }
        }

        public ExportRow ExportRow
        {
            get => _exportRow;
            set
            {
                _exportRow = value;
                OnPropertyChanged();
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string WindowTitle
        {
            get
            {
                if (IsEditing)
                    return "Edit";
                else
                    return "Add";
            }
        }

        public string AgreeLabel
        {
            get
            {
                if (IsEditing)
                    return "Save";
                else
                    return "Add";
            }
        }
    }
}
