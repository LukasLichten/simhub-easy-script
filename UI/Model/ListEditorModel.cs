﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Easy_Script.UI.Model
{
    internal class ListEditorModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<PropertyRow> _properties = new ObservableCollection<PropertyRow>();

        public ObservableCollection<PropertyRow> Properties
        {
            get => _properties;
            
            set
            {
                _properties = value;
                OnPropertyChanged();
            }
        }

        private ListMode _mode;

        public ListMode ListMode
        {
            get => _mode;

            set
            {
                _mode = value;
                OnPropertyChanged();

                //Switching Things On And Off
                switch (_mode)
                {
                    case ListMode.Event:
                        ColumnWidth = new int[] { 30, 150, 510, 55, 0, 85, 0 };
                        EditButtonText = new string[]{ "Edit Condition", "Edit Trigger", "Edit Perf", "Condition" };
                        EditButtonVisible = new Visibility[] { Visibility.Visible, Visibility.Visible, Visibility.Visible };
                        break;
                    case ListMode.Logging:
                        ColumnWidth = new int[] { 30, 150, 510, 0, 0, 0, 140 };
                        EditButtonText = new string[] { "Edit Output", "Select File", "", "Output" };
                        EditButtonVisible = new Visibility[] { Visibility.Visible, Visibility.Visible, Visibility.Hidden };
                        break;
                    case ListMode.Property:
                        ColumnWidth = new int[] { 30, 142, 508, 0, 65, 85, 0 };
                        EditButtonText = new string[] { "Edit Output", "Change Type", "Edit Perf", "Output" };
                        EditButtonVisible = new Visibility[] { Visibility.Visible, Visibility.Visible, Visibility.Visible };
                        break;
                }
            }
        }

        private int[] _columnWidth = { 10 ,180, 500, 55, 75, 0, 0 }; //Total has to be 830

        public int[] ColumnWidth
        {
            get => (int[]) _columnWidth.Clone();
            private set
            {
                if (value.Length != 7)
                    return;

                _columnWidth = (int[]) value.Clone();
                OnPropertyChanged();
            }
        }

        private string[] _editButtonText = { "Edit Condition", "Edit Trigger", "Edit Perf", "Condition" };

        public string[] EditButtonText
        {
            get => (string[])_editButtonText.Clone();
            private set
            {
                if (value.Length != 4)
                    return;

                _editButtonText = (string[])value.Clone();
                OnPropertyChanged();
            }
        }

        private Visibility[] _editButtonVisible = { Visibility.Visible, Visibility.Visible, Visibility.Visible };

        public Visibility[] EditButtonVisible
        {
            get => (Visibility[])_editButtonVisible.Clone();
            private set
            {
                if (value.Length != 3)
                    return;

                _editButtonVisible = (Visibility[])value.Clone();
                OnPropertyChanged();
            }
        }

        

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    internal class PropertyRow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _enabled;

        private Action<bool> _setEnabled;

        private string _name;

        private string _property;

        private Helper.EventTriggers _trigger;

        private int _updateSkip;

        private bool _parallel;

        private string _fileName;

        private Helper.OutputType _outputType;

        public string Name
        {
            get => _name;

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public bool Enabled
        {
            get => _enabled;

            set
            {
                _enabled = value;
                OnPropertyChanged();

                if (_setEnabled != null)
                    _setEnabled.Invoke(_enabled);
            }
        }

        public string Property
        {
            get => _property;

            set
            {
                value = value.Trim();

                if (Regex.Matches(value, "\n\r|\r\n|\n|\r").Count > 4)
                {
                    int last = value.LastIndexOfAny(new char[] { '\n', '\r' });

                    string firstLine = value.Substring(0, value.IndexOfAny(new char[]{ '\n', '\r' }));
                    string lastLine = value.Substring(last, value.Length-last);

                    value = firstLine + "\n..." + lastLine;
                }

                _property = value;
                OnPropertyChanged();
            }
        }

        public Helper.EventTriggers Trigger
        {
            get => _trigger;

            set
            {
                _trigger = value;
                OnPropertyChanged();
            }
        }

        public Helper.OutputType OutputType
        {
            get => _outputType;

            set
            {
                _outputType = value;
                OnPropertyChanged();
            }
        }

        public int UpdateSkip
        {
            get => _updateSkip;

            set
            {
                _updateSkip = value;
                OnPropertyChanged();
            }
        }

        public bool Parallel
        {
            get => _parallel;

            set
            {
                _parallel = value;
                OnPropertyChanged();
            }
        }

        public string FileName
        {
            get => _fileName;
            set
            {
                _fileName = value;
                OnPropertyChanged();
            }
        }

        public string GetTriggerTooltip
        {
            get => "If the Event should trigger on Rising (false to true), Falling (true to false) or Both";
        }

        public string GetUpdateSkipTooltip
        {
            get => "UpdateSkip, number of data updates prior running the script again";
        }

        public string GetParallelTooltip
        {
            get => "Parallel execute the script, may lead to delayed respondes, also only improves Performance on larger scripts (and js)";
        }

        public PropertyRow(Action<bool> enableAction)
        {
            _setEnabled = enableAction;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum ListMode
    {
        Event, Logging, Property
    }
}
