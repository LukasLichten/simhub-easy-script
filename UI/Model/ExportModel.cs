﻿using Easy_Script.Helper;
using Easy_Script.UI.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.UI.Model
{
    public class ExportModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<ExportRow> _rows = new ObservableCollection<ExportRow>();

        public ObservableCollection<ExportRow> Rows
        {
            get => _rows;

            set
            {
                _rows = value;
                OnPropertyChanged();
            }
        }

        private bool _import = false;

        public bool Import
        {
            get => _import;
            set
            {
                _import = value;
                OnPropertyChanged();
                OnPropertyChanged("Title");
            }
        }

        public string Title
        {
            get
            {
                if (Import)
                    return "Import";
                else
                    return "Export";
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ExportRow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _name = "";
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        private ListMode _listMode = ListMode.Event;
        public ListMode Mode
        {
            get => _listMode;
            set
            {
                _listMode = value;
                OnPropertyChanged();
            }
        }

        public ExportedScript ImportScript { get; set; }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
