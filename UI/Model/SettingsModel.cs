﻿using Easy_Script.UI.View;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Easy_Script.UI.Model
{
    internal class SettingsModel :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _changed;

        private int _suspendFailedScriptExecutionSec;

        public int SuspendFailedScriptExecutionSec
        {
            get => _suspendFailedScriptExecutionSec;
            set
            {
                _suspendFailedScriptExecutionSec = value;
                OnPropertyChanged();
                Changed = true;
            }
        }

        public string GetSuspendTooltip
        {
            get => "The Time to suspend the execution of a script when it fails";
        }

        public bool Changed
        {
            get => _changed;
            set
            {
                _changed = value;
                OnPropertyChanged();
            }
        }

        public ListEditor[] ListEditors { get; set; }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
