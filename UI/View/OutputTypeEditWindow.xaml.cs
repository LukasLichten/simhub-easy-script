﻿using Easy_Script.Helper;
using Easy_Script.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// Interaction logic for PropertyEditWindow.xaml
    /// </summary>
    public partial class OutputTypeEditWindow : Window
    {
        private EasyScript Plugin;

        private EditorModel Model;

        public OutputTypeEditWindow()
        {
            InitializeComponent();
        }

        internal void Init(EasyScript plugin, EditorModel model)
        {
            Plugin = plugin;
            Model = model;

            this.DataContext = Model;
        }

        private void BtnAgree_Click(object sender, RoutedEventArgs e)
        {
            if (Model.IsEditing)
            {
                Script script = GetScript(Model.Row);

                if (Model.Mother.ListMode == ListMode.Property)
                {
                    PropertyScript propScript = script as PropertyScript;

                    propScript.OutputType = Model.OutputType;
                    Model.Row.OutputType = Model.OutputType;
                }
                
            }

            this.Close();
        }

        private Script GetScript(PropertyRow row)
        {
            if (row == null)
                return null;

            switch (Model.Mother.ListMode)
            {
                case ListMode.Event: return Plugin.Settings.CustomEvents[row.Name];
                case ListMode.Logging: return Plugin.Settings.CustomLogging[row.Name];
                case ListMode.Property: return Plugin.Settings.CustomProperty[row.Name];
                default: return null;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
