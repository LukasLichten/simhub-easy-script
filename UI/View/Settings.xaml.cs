﻿using Easy_Script.Helper;
using Easy_Script.UI.Model;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MessageBox = System.Windows.Forms.MessageBox;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;
using DialogResult = System.Windows.Forms.DialogResult;
using SimHub.Plugins;
using Lichten.SimHubHelper;
using SimHub;
using log4net.Plugin;
using System.Windows.Markup;
using System.Linq;
using System.Collections.Generic;
using FMOD;
using System.Xml.Linq;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// A simple Settings panel, with some examples for handling booleans, text and numbers.
    /// And able to revert unsaved changes
    /// </summary>
    public partial class Settings : UserControl
    {
        private EasyScript Main { get; set; }

        private SettingsModel Model { get; set; }

        public Settings()
        {
            InitializeComponent();
        }

        public Settings(EasyScript main) : base()
        {
            // You can simplify this if you intend to just use this settings panel
            Init(main);
        }

        internal void Init(EasyScript main)
        {
            Main = main;

            LoadData();
        }

        internal void Init(EasyScript main, ListEditor[] list)
        {
            Model = new SettingsModel()
            {
                ListEditors = list
            };

            Init(main);
        }

        private void LoadData()
        {
            ListEditor[] list = null;
            if (Model != null)
                list = Model.ListEditors;

            //Generating DataModel from the settings of Main
            Model = new SettingsModel()
            {
                SuspendFailedScriptExecutionSec = Main.Settings.SuspendFailedScriptExecutionSec,
                ListEditors = list,

                Changed = false, //Reset to disable the Apply/Revert buttons
            };

            DataContext = Model;
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Main.Settings.SuspendFailedScriptExecutionSec = Model.SuspendFailedScriptExecutionSec;



            Model.Changed = false; //Reset to disable the Apply/Revert buttons

            //If you need to update some property (like resizing an array, reconnect a web client, etc) you should call the function to do that here
        }

        private void BtnRevert_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void BtnImport_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Json File (*.json)|*.json",
                Title = "Select Config To Import"
            };

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string data = ReadJSONFromFile(dialog.FileName);

                if (data == null)
                {
                    MessageBox.Show("File did not contain any Data or did not exist!", "No Data", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return;
                }

                var list = JsonConvert.DeserializeObject<ExportedScript[]>(data, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });

                var window = new ExportWindow() { ListEditors = Model.ListEditors };
                window.Init(Main, list);
                window.Show();
            }

            
        }

        private void BtnExport_Click(object sender, RoutedEventArgs e)
        {
            var window = new ExportWindow();
            window.Init(Main);
            window.Show();
        }

        internal static void ExportData(string path, ExportedScript[] exportedScripts)
        {
            string data = JsonConvert.SerializeObject(exportedScripts, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });

            WriteJSONToFile(path, data);
        }

        public static void WriteJSONToFile(string path, string dataToWrite)
        {
            StreamWriter writer = null;

            try
            {
                writer = new StreamWriter(path, false, Encoding.UTF8);
                writer.Write(dataToWrite);
                writer.Flush();
            }
            catch (IOException ex)
            {
                SimHub.Logging.Current.Error("EasyScript: Exception thrown when trying save Delta Data: " + ex.Message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public static string ReadJSONFromFile(string path)
        {
            StreamReader reader = null;
            string output = null;

            if (!File.Exists(path))
            {
                SimHub.Logging.Current.Info("EasyScript: Delta Data does not exist, no data will be loaded");
                return output;
            }

            try
            {
                reader = new StreamReader(path, Encoding.UTF8);
                output = reader.ReadToEnd();
            }
            catch (IOException ex)
            {
                SimHub.Logging.Current.Error("EasyScript: Exception thrown when trying read Delta Data: " + ex.Message);
                output = null;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return output;
        }
    }

    public class ExportedScript
    {
        public string Name { get; set; }

        public Script Script { get; set; }
    }
}
