﻿using Easy_Script.Helper;
using Easy_Script.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// Interaction logic for PropertyEditWindow.xaml
    /// </summary>
    public partial class RenamerWindow : Window
    {
        private EasyScript Plugin;

        private EditorModel Model;

        public RenamerWindow()
        {
            InitializeComponent();
        }

        internal void Init(EasyScript plugin, EditorModel model)
        {
            Plugin = plugin;
            Model = model;

            this.DataContext = Model;
        }

        private void BtnAgree_Click(object sender, RoutedEventArgs e)
        {
            //Renaming an import
            if (Model.IsImporting)
            {
                if (!CheckIfNameAvailable())
                    return;

                Model.ExportRow.Name = Model.Name;

                this.Close();
                return;
            }

            //Normal Renaming
            if (Model.IsEditing)
            {
                Script script = GetScript(Model.Row);

                if (Model.Row.Name != Model.Name)
                {
                    if (!CheckIfNameAvailable())
                        return;

                    switch (Model.Mother.ListMode)
                    {
                        case ListMode.Event:
                            Plugin.Settings.CustomEvents.Remove(Model.Row.Name);
                            ((EventScript)script).ScriptEventHelper.EventName = Model.Name;
                            ((EventScript)script).ScriptEventHelper.RegisterEvent();
                            break;
                        case ListMode.Logging:
                            Plugin.Settings.CustomLogging.Remove(Model.Row.Name);

                            //Removing Action for PluginManager
                            Plugin.PluginManager.ClearActions(Plugin.Logging.GetType(), Model.Row.Name);
                            break;
                        case ListMode.Property:
                            Plugin.Settings.CustomProperty.Remove(Model.Row.Name);

                            //We have to remove all properties and regenerate
                            Plugin.PluginManager.ClearProperties(Plugin.Propertys.GetType());
                            foreach (var prop in Plugin.Settings.CustomProperty)
                                prop.Value.Generate(Plugin.PluginManager, prop.Key, Plugin);

                            ((PropertyScript)script).Generate(Plugin.PluginManager, Model.Name, Plugin);
                            break;
                    }

                    AddField(script);

                    Model.Row.Name = Model.Name;
                    Model.Row.Property = script.Expression;
                }
                else
                {
                    //no Renaming happened, nothing to do
                }
            }
            else
            {
                if (!CheckIfNameAvailable())
                    return;

                Script script = null;

                switch (Model.Mother.ListMode)
                {
                    case ListMode.Event:
                        EventScript ev = new EventScript();
                        ev.Generate(Plugin, Model.Name);
                        script = ev;
                        break;
                    case ListMode.Logging:
                        LoggingScript log = new LoggingScript();
                        log.ActionHelper = new Lichten.SimHubHelper.ActionHelper(log.Write);
                        log.NCalcEngine = Plugin.NCalcEngine;
                        script = log;
                        break;
                    case ListMode.Property:
                        PropertyScript property = new PropertyScript();
                        property.Generate(Plugin.PluginManager, Model.Name, Plugin);
                        script = property;
                        break;
                }

                AddField(script);
                Model.Mother.Properties.Add(new PropertyRow(script.SetEnabled) { Enabled = script.Enabled, Name = Model.Name, Property = script.Expression });
            }


            this.Close();
        }

        private Script GetScript(PropertyRow row)
        {
            if (row == null)
                return null;

            switch (Model.Mother.ListMode)
            {
                case ListMode.Event: return Plugin.Settings.CustomEvents[row.Name];
                case ListMode.Logging: return Plugin.Settings.CustomLogging[row.Name];
                case ListMode.Property: return Plugin.Settings.CustomProperty[row.Name];
                default: return null;
            }
        }

        private bool CheckIfNameAvailable()
        {
            string[] names = new string[0];

            switch (Model.Mother.ListMode)
            {
                case ListMode.Event:
                    var evKeys = Plugin.Settings.CustomEvents.Keys;
                    names = evKeys.ToArray();
                    break;
                case ListMode.Logging:
                    var logKeys = Plugin.Settings.CustomLogging.Keys;
                    names = logKeys.ToArray();
                    break;
                case ListMode.Property:
                    var propKeys = Plugin.Settings.CustomProperty.Keys;
                    names = propKeys.ToArray();
                    break;
            }

            string lowerCaseName = Model.Name.ToLower();

            foreach (var key in names)
            {
                //We won't allow keys with the same spelling but different case
                if (key.ToLower() == lowerCaseName)
                {
                    MessageBox.Show(Model.Mother.ListMode + " name " + Model.Name + " is already in use. Pick another Name", Model.Mother.ListMode + " Name Collision", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            return true;
        }

        private void AddField(Script script)
        {
            switch (Model.Mother.ListMode)
            {
                case ListMode.Event:
                    Plugin.Settings.CustomEvents.Add(Model.Name, (EventScript)script);
                    break;
                case ListMode.Logging:
                    Plugin.Settings.CustomLogging.Add(Model.Name, (LoggingScript)script);
                    Plugin.PluginManager.AddAction(Model.Name, Plugin.Logging.GetType(), ((LoggingScript)script).ActionHelper.TriggerAction);
                    break;
                case ListMode.Property:
                    Plugin.Settings.CustomProperty.Add(Model.Name, (PropertyScript)script);
                    break;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
