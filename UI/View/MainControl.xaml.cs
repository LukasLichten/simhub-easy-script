﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Easy_Script.UI.View;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// This serves to offer a multi tab expierence, so you can segment different settings into different pages
    /// </summary>
    public partial class MainControl : UserControl
    {
        
        public MainControl(EasyScript main)
        {
            InitializeComponent();

            //We now need to pass the additional data into the individual pages, for them to build their data model
            
            EventsView.Init(main, Model.ListMode.Event);
            PropertysView.Init(main, Model.ListMode.Property);
            LoggingView.Init(main, Model.ListMode.Logging);

            SettingsView.Init(main, new ListEditor[] { EventsView, PropertysView, LoggingView });
        }
    }
}
