﻿using Easy_Script.Helper;
using Easy_Script.UI.Model;
using Lichten.SimHubHelper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// Interaction logic for PropertyEditWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        private EasyScript Plugin;

        private ExportModel Model;

        public ListEditor[] ListEditors { get; set; }

        public ExportWindow()
        {
            InitializeComponent();

            BtnRename.IsEnabled = false;
        }

        internal void Init(EasyScript plugin)
        {
            Plugin = plugin;
            Model = new ExportModel();

            foreach (var item in Plugin.Settings.CustomEvents)
            {
                AddRow(item.Key, ListMode.Event);
            }
            foreach (var item in Plugin.Settings.CustomProperty)
            {
                AddRow(item.Key, ListMode.Property);
            }
            foreach (var item in Plugin.Settings.CustomLogging)
            {
                AddRow(item.Key, ListMode.Logging);
            }

            BtnRename.Visibility = Visibility.Hidden;

            this.DataContext = Model;
        }

        internal void Init(EasyScript plugin, ExportedScript[] toImport)
        {
            Plugin = plugin;
            Model = new ExportModel()
            {
                Import = true,
            };

            foreach (var item in toImport)
            {
                if (item.Script is EventScript)
                    AddRow(item.Name, ListMode.Event, item);
                else if (item.Script is PropertyScript)
                    AddRow(item.Name, ListMode.Property, item);
                else if (item.Script is LoggingScript)
                    AddRow(item.Name, ListMode.Logging, item);
            }

            BtnRename.Visibility = Visibility.Visible;

            this.DataContext = Model;
        }

        private void AddRow(string name, ListMode mode) { AddRow(name, mode, null); }

        private void AddRow(string name, ListMode mode, ExportedScript script)
        {
            var row = new ExportRow()
            {
                IsSelected = Model.Import, //When importing default to selected, when exporting default to deselected
                Name = name,
                Mode = mode,
                ImportScript = script
            };
            Model.Rows.Add(row);
        }

        private void BtnAgree_Click(object sender, RoutedEventArgs e)
        {
            if (Model.Import)
                Import();
            else
                Export();

            //Exporting
            void Export()
            {
                var dialog = new System.Windows.Forms.SaveFileDialog
                {
                    Filter = "Json File (*.json)|*.json",
                    Title = "Save Export Json"
                };

                var result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    List<ExportedScript> exportedScripts = new List<ExportedScript>();

                    foreach (var item in Model.Rows)
                    {
                        if (item.IsSelected)
                        {
                            var script = GetScript(item);

                            exportedScripts.Add(new ExportedScript() { Name = item.Name, Script = script });
                        }
                    }

                    Settings.ExportData(dialog.FileName, exportedScripts.ToArray());

                    this.Close();
                }
            }

            //Importing
            void Import()
            {
                var add = new List<ExportRow>();

                //Filtering out those not selected, checking for name collisions
                foreach (var item in Model.Rows)
                {
                    if (item.IsSelected)
                    {
                        if (CheckForDublicates(item.Mode, item.Name))
                        {
                            var result = System.Windows.Forms.MessageBox.Show($"You are trying to import {item.Mode} {item.Name}, which already exists.\n\nDo you want to override it?", "Name Collision", System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.None);

                            if (result == System.Windows.Forms.DialogResult.Cancel)
                                return;
                        }

                        add.Add(item);
                    }
                }

                if (add.Count > 0)
                {
                    //Importing
                    foreach (var item in add)
                    {
                        if (CheckForDublicates(item.Mode, item.Name))
                        {
                            //We override by deleting the existing one
                            switch (item.Mode)
                            {
                                case ListMode.Event:
                                    Plugin.Settings.CustomEvents.Remove(Plugin.Settings.CustomEvents.ToList().Find(x => x.Key.ToLower() == item.Name.ToLower()).Key);
                                    break;
                                case ListMode.Property:
                                    Plugin.Settings.CustomProperty.Remove(Plugin.Settings.CustomProperty.ToList().Find(x => x.Key.ToLower() == item.Name.ToLower()).Key);
                                    break;
                                case ListMode.Logging:
                                    Plugin.Settings.CustomLogging.Remove(Plugin.Settings.CustomLogging.ToList().Find(x => x.Key.ToLower() == item.Name.ToLower()).Key);
                                    break;
                            }
                        }

                        //Adding the script
                        if (item.ImportScript.Script is EventScript evScript)
                        {
                            evScript.Generate(Plugin, item.Name);

                            Plugin.Settings.CustomEvents.Add(item.Name, evScript);

                        }
                        else if (item.ImportScript.Script is PropertyScript propScript)
                        {
                            propScript.Generate(Plugin.PluginManager, item.Name, Plugin);

                            Plugin.Settings.CustomProperty.Add(item.Name, propScript);
                        }
                        else if (item.ImportScript.Script is LoggingScript logScript)
                        {
                            logScript.NCalcEngine = Plugin.NCalcEngine;
                            logScript.ActionHelper = new ActionHelper(logScript.Write);
                            Plugin.PluginManager.AddAction(item.Name, Plugin.Logging.GetType(), logScript.ActionHelper.TriggerAction);
                            Plugin.Settings.CustomLogging.Add(item.Name, logScript);
                        }
                    }

                    //Update UI
                    foreach (var view in ListEditors)
                    {
                        view.Init(Plugin, view.Model.ListMode);
                    }
                }

                this.Close();
            }

            bool CheckForDublicates(ListMode mode, string name)
            {
                string[] names = new string[0];

                switch (mode)
                {
                    case ListMode.Event:
                        var evKeys = Plugin.Settings.CustomEvents.Keys;
                        names = evKeys.ToArray();
                        break;
                    case ListMode.Logging:
                        var logKeys = Plugin.Settings.CustomLogging.Keys;
                        names = logKeys.ToArray();
                        break;
                    case ListMode.Property:
                        var propKeys = Plugin.Settings.CustomProperty.Keys;
                        names = propKeys.ToArray();
                        break;
                }

                string lowerCaseName = name.ToLower();

                foreach (var key in names)
                {
                    //We won't allow keys with the same spelling but different case
                    if (key.ToLower() == lowerCaseName)
                    {
                        return true;
                    }
                }

                return false;

            }
        }

        private Script GetScript(ExportRow row)
        {
            if (row == null)
                return null;

            switch (row.Mode)
            {
                case ListMode.Event: return Plugin.Settings.CustomEvents[row.Name];
                case ListMode.Logging: return Plugin.Settings.CustomLogging[row.Name];
                case ListMode.Property: return Plugin.Settings.CustomProperty[row.Name];
                default: return null;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnRename_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null || !Model.Import)
                return;

            ListEditor mother = null;
            foreach (var editors in ListEditors)
            {
                if (editors.Model.ListMode == item.Mode)
                {
                    mother = editors;
                    break;
                }
            }

            EditorModel editorModel = new EditorModel()
            {
                IsImporting = true,
                IsEditing = true,
                Name = item.Name,
                ExportRow = item,
                Mother = mother.Model
            };

            var window = new RenamerWindow();
            window.Init(Plugin, editorModel);
            window.Show();
        }

        private void LvPropertys_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetSelectedIndex();
        }

        private ExportRow GetSelectedIndex()
        {
            object item = LvPropertys.SelectedItem;

            if (item is ExportRow)
            {
                BtnRename.IsEnabled = true;

                return item as ExportRow;
            }
            else
            {
                BtnRename.IsEnabled = false;
            }

            return null;
        }
    }
}
