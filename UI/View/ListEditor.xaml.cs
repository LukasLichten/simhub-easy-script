﻿using Easy_Script.Helper;
using Easy_Script.UI.Model;
using SimHub.Plugins.OutputPlugins.Dash.GLCDTemplating;
using SimHub.Plugins.OutputPlugins.EditorControls;
using SimHub.Plugins.OutputPlugins.GraphicalDash.Models;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Easy_Script.UI.View
{
    /// <summary>
    /// Interaction logic for PropertiesSettingUI.xaml
    /// </summary>
    public partial class ListEditor : UserControl
    { 
        private EasyScript Plugin { get; set; }

        internal ListEditorModel Model { get; private set; }


        public ListEditor()
        {
            InitializeComponent();
            BtnEdit.IsEnabled = false;
            BtnEdit2.IsEnabled = false;
            BtnEditExtra.IsEnabled = false;
            BtnDelete.IsEnabled = false;
            BtnRename.IsEnabled = false;
        }

        internal void Init(EasyScript plugin, ListMode mode)
        {
            Plugin = plugin;
            this.Model = new ListEditorModel()
            {
                Properties = new System.Collections.ObjectModel.ObservableCollection<PropertyRow>(),
                ListMode = mode,
            };

            if (Model.ListMode == ListMode.Event)
            {
                var dataPoints = Plugin.Settings.CustomEvents;
                foreach (var point in dataPoints)
                {
                    PropertyRow row = new PropertyRow(point.Value.SetEnabled)
                    {
                        Enabled = point.Value.Enabled,
                        Name = point.Key,
                        Property = point.Value.Expression,
                        Trigger = point.Value.EventTrigger,
                        Parallel = point.Value.ParallelExecution,
                        UpdateSkip = point.Value.UpdateSkip,
                    };
                    Model.Properties.Add(row);
                }
            }
            else if (Model.ListMode == ListMode.Logging)
            {
                var dataPoints = Plugin.Settings.CustomLogging;
                foreach (var point in dataPoints)
                {
                    PropertyRow row = new PropertyRow(point.Value.SetEnabled)
                    {
                        Enabled = point.Value.Enabled,
                        Name = point.Key,
                        Property = point.Value.Expression,
                        FileName = System.IO.Path.GetFileName(point.Value.FilePath),
                    };
                    Model.Properties.Add(row);
                }
            }
            else if (Model.ListMode == ListMode.Property)
            {
                var dataPoints = Plugin.Settings.CustomProperty;
                foreach (var point in dataPoints)
                {
                    PropertyRow row = new PropertyRow(point.Value.SetEnabled)
                    {
                        Enabled = point.Value.Enabled,
                        Name = point.Key,
                        Property = point.Value.Expression,
                        OutputType = point.Value.OutputType,
                        Parallel = point.Value.ParallelExecution,
                        UpdateSkip = point.Value.UpdateSkip,
                    };
                    Model.Properties.Add(row);
                }
            }

            this.DataContext = this.Model;
        }

        private void LvPropertys_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetSelectedIndex();
        }

        private PropertyRow GetSelectedIndex()
        {
            object item = LvPropertys.SelectedItem;

            if (item is PropertyRow)
            {
                BtnDelete.IsEnabled = true;
                BtnEdit.IsEnabled = true;
                BtnRename.IsEnabled = true;
                BtnEdit2.IsEnabled = true;
                BtnEditExtra.IsEnabled = true;

                return item as PropertyRow;
            }
            else
            {
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnRename.IsEnabled = false;
                BtnEdit2.IsEnabled = false;
                BtnEditExtra.IsEnabled = false;
            }

            return null;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            var result = MessageBox.Show("Do you really want to Delete this " + Model.ListMode + "?\n\n" + item.Name + "\n" + item.Property, "Are you sure?", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                bool success = false;

                switch (Model.ListMode)
                {
                    case ListMode.Event:
                        success = Plugin.Settings.CustomEvents.Remove(item.Name);
                        //No removing of evenets possible
                        break;
                    case ListMode.Logging:
                        success = Plugin.Settings.CustomLogging.Remove(item.Name);

                        //Removing Action
                        Plugin.PluginManager.ClearActions(Plugin.Logging.GetType(), item.Name);
                        break;
                    case ListMode.Property:
                        success = Plugin.Settings.CustomProperty.Remove(item.Name);

                        //We have to remove all properties and regenerate
                        Plugin.PluginManager.ClearProperties(Plugin.Propertys.GetType());
                        foreach (var prop in Plugin.Settings.CustomProperty)
                            prop.Value.Generate(Plugin.PluginManager, prop.Key, Plugin);
                        break;
                }

                if (success)
                    Model.Properties.Remove(item);
                else
                    MessageBox.Show("Something went wrong deleting the " + item.Name + ". It might have been already deleted", "Something went wrong", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            //Deals with the Scripting
            var item = GetSelectedIndex();

            var script = GetScript(item);

            if (script == null)
                return;

            try
            {
                BindingEditor editor = new BindingEditor(Plugin.NCalcEngine);

                ExpressionValue value = new ExpressionValue
                {
                    Expression = script.Expression,
                    Interpreter = script.Interpreter
                };

                DashboardBindingData dashboardBindingData = new DashboardBindingData()
                {
                    Formula = value,
                    Mode = BindingMode.Formula,
                    TargetPropertyName = item.Name,
                    TargetType = typeof(bool),
                    AllowText = true,
                    FormatString = script.Format,
                };
                editor.DataContext = dashboardBindingData;

                editor.ShowDialog(this, () =>
                {
                    if (editor.DialogResult != System.Windows.Forms.DialogResult.OK)
                        return;

                    if (editor.DataContext is DashboardBindingData bindingData && bindingData.Mode == BindingMode.Formula)
                    {
                        script.Expression = bindingData.Formula.Expression;
                        script.Interpreter = bindingData.Formula.Interpreter;
                        script.Format = bindingData.FormatString;

                        item.Property = script.Expression;
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open Editor.\nFor further information refer to log.","Error Occured", MessageBoxButton.OK, MessageBoxImage.Error);
                
                SimHub.Logging.Current.Error(ex);
            }
        }

        private Script GetScript(PropertyRow row)
        {
            if (row == null)
                return null;

            switch (Model.ListMode)
            {
                case ListMode.Event: return Plugin.Settings.CustomEvents[row.Name];
                case ListMode.Logging: return Plugin.Settings.CustomLogging[row.Name];
                case ListMode.Property: return Plugin.Settings.CustomProperty[row.Name];
                default: return null;
            }
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            var addModel = new EditorModel()
            {
                IsEditing = false,
                Mother = Model,
                Name = ""
            };

            var window = new RenamerWindow();
            window.Init(Plugin, addModel);
            window.Show();
        }

        private void BtnRename_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            var editModel = new EditorModel()
            {
                IsEditing = true,
                Mother = Model,
                Row = item
            };

            var window = new RenamerWindow();
            window.Init(Plugin, editModel);
            window.Show();
        }

        private void BtnEdit2_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            if (Model.ListMode == ListMode.Event)
            {
                var editModel = new EditorModel()
                {
                    IsEditing = true,
                    Mother = Model,
                    Row = item,
                    Trigger = item.Trigger,
                };

                var window = new TriggerEditWindow();
                window.Init(Plugin, editModel);
                window.Show();
            }
            else if (Model.ListMode == ListMode.Logging)
            {
                System.Windows.Forms.SaveFileDialog fileDialog = new System.Windows.Forms.SaveFileDialog()
                {
                    AddExtension = true,
                    OverwritePrompt = false,
                    Filter = "txt file (*.txt)|*.txt|log file (*.log)|*.log|All files (*.*)|*.*",
                    Title = "Set " + item.Name + " log Destination"
                };

                LoggingScript lscript = GetScript(item) as LoggingScript;

                if (!string.IsNullOrEmpty(lscript.FilePath))
                {
                    fileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(lscript.FilePath);
                    fileDialog.FileName = System.IO.Path.GetFileName(lscript.FilePath);

                    switch (System.IO.Path.GetExtension(lscript.FilePath).ToLower())
                    {
                        case "txt":
                            fileDialog.FilterIndex = 0; break;
                        case "log":
                            fileDialog.FilterIndex = 1; break;
                        default:
                            fileDialog.FilterIndex = 2; break;
                    }
                }
                

                var result = fileDialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    lscript.FilePath = fileDialog.FileName;
                    item.FileName = System.IO.Path.GetFileName(lscript.FilePath);
                }
            }
            else if (Model.ListMode == ListMode.Property)
            {
                var editModel = new EditorModel()
                {
                    IsEditing = true,
                    Mother = Model,
                    Row = item,
                    OutputType = item.OutputType,
                };

                var window = new OutputTypeEditWindow();
                window.Init(Plugin, editModel);
                window.Show();
            }
        }

        private void BtnEditExtra_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            if (Model.ListMode == ListMode.Event || Model.ListMode == ListMode.Property)
            {
                var editModel = new EditorModel()
                {
                    IsEditing = true,
                    Mother = Model,
                    Row = item,
                    Parallel = item.Parallel,
                    UpdateSkip = item.UpdateSkip,
                };

                var window = new PerfEditWindow();
                window.Init(Plugin, editModel);
                window.Show();
            }
        }
    }
}
