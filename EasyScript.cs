﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Collections.Generic;
using Lichten.SimHubHelper;
using System.Windows.Media;
using Easy_Script.UI.View;
using SimHub.Plugins.OutputPlugins.Dash.TemplatingCommon;
using SimHub.Plugins.OutputPlugins.Dash.GLCDTemplating;
using Easy_Script.Helper;

namespace Easy_Script
{

    [PluginName("Easy Script")]
    [PluginAuthor("Lukas Lichten")]
    [PluginDescription("Allows you to create simple actions, events, properties and logging using SimHub's ncalc/js engine")]
    public class EasyScript : IPlugin, IDataPlugin, IWPFSettingsV2 
    {
        public const string EXEC_TIME = "ExecutionTimeInMs";

        public ES_Settings Settings { get; set; }

        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        /// <summary>
        /// Gets the left menu icon. Icon must be 24x24 and compatible with black and white display.
        /// </summary>
        public ImageSource PictureIcon => this.ToIcon(Properties.Resources.icon);

        /// <summary>
        /// Gets a short plugin title to show in left menu. Return null if you want to use the title as defined in PluginName attribute.
        /// </summary>
        public string LeftMenuTitle => "Easy Script";

        public NCalcEngineBase NCalcEngine { get; private set; }

        public EasyScriptLogging Logging { get; private set; }
        public EasyScriptPropertys Propertys { get; private set; }

        /// <summary>
        /// Called after plugins startup
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            PluginManager = pluginManager;

            // Load settings
            Settings = this.ReadCommonSettings<ES_Settings>("EasyScript", () => new ES_Settings());

            NCalcEngine = new NCalcEngineBase()
            {
                AllowLogging = true,
                UseCache = true,
            };
            Logging = new EasyScriptLogging();
            Propertys = new EasyScriptPropertys();

            //pluginManager.NewLap += new PluginManager.NewLapDelegate(NewLapUpdate);

            foreach (var item in Settings.CustomEvents)
            {
                item.Value.Generate(this, item.Key);
            }

            foreach (var item in Settings.CustomLogging)
            {
                item.Value.NCalcEngine = NCalcEngine;
                item.Value.ActionHelper = new ActionHelper(item.Value.Write);
                pluginManager.AddAction(item.Key, Logging.GetType(), item.Value.ActionHelper.TriggerAction);
            }

            foreach (var item in Settings.CustomProperty)
            {
                item.Value.Generate(pluginManager, item.Key, this);
            }

            pluginManager.AddProperty(EXEC_TIME, this.GetType(), double.MaxValue.GetType(), "Time each Update cycal spend on evaluating formulas");
        }

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            foreach (var item in Settings.CustomEvents)
            {
                item.Value.ScriptEventHelper.Update(pluginManager);
            }
            foreach (var item in Settings.CustomProperty)
            {
                item.Value.Update();
            }


            stopwatch.Stop();
            pluginManager.SetPropertyValue(EXEC_TIME, this.GetType(), stopwatch.Elapsed.TotalMilliseconds);
        }

        /// <summary>
        /// Registered on init, will trigger when a lap is completed
        /// You can remove it's registration from init and then you can remove the function
        /// </summary>
        /// <param name="completedLapNumber">This </param>
        /// <param name="testLap"></param>
        /// <param name="manager"></param>
        /// <param name="data"></param>
        public void NewLapUpdate(int completedLapNumber, bool testLap, PluginManager manager, ref GameData data)
        {
            // data.NewData.CompletedLaps == completedLapNumber + 1;
        }

        /// <summary>
        /// Called at plugin manager stop, close/displose anything needed here !
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            this.SaveCommonSettings<ES_Settings>("EasyScript", Settings);
        }

        /// <summary>
        /// Used to gather 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            // This is a multitab control panel
            return new MainControl(this);
        }


    }

    public class EasyScriptLogging
    {
        
    }

    public class EasyScriptPropertys
    {

    }
}