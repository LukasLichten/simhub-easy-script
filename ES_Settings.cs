﻿using Easy_Script.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script
{
    public class ES_Settings
    {
        public int SuspendFailedScriptExecutionSec { get; set; } = 30;
        

        public Dictionary<string, EventScript> CustomEvents { get; set; } = new Dictionary<string, EventScript>();

        public Dictionary<string, LoggingScript> CustomLogging { get; set; } = new Dictionary<string, LoggingScript>();

        public Dictionary<string, PropertyScript> CustomProperty { get; set; } = new Dictionary<string, PropertyScript>();
    }
}
