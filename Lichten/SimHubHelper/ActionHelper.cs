﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// Simple Wrapper for Actions
    /// </summary>
    public class ActionHelper
    {
        public Action Method { get; set; }

        public ActionHelper() { }

        public ActionHelper(Action method) : this()
        {
            Method = method;
        }

        public void TriggerAction(PluginManager pluginManager, string data)
        {
            try
            {
                //Method.Invoke();
                Method.BeginInvoke(null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }
    }
}
