﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// This Increases and decreases a value via Actions.
    /// </summary>
    public class IterationActionHelper
    {
        public Action<int> Method { get; set; }
        public int StepSize { get; set; }
        public CurrentValueGetter CurrentValue { get; set; }

        public IterationActionHelper() { }
        public IterationActionHelper(Action<int> action, CurrentValueGetter currentValueGetter, int stepSize)
        {
            Method = action;
            StepSize = stepSize;
            CurrentValue = currentValueGetter;
        }

        protected virtual void HandleSwitch(int step)
        {
            int value = CurrentValue.Invoke();

            if (value >= 0)
            {
                Method.Invoke(value + step);
            }
        }

        public void TriggerIncrease(PluginManager pluginManager, string data)
        {
            try
            {
                Action<int> act = HandleSwitch;
                act.BeginInvoke(StepSize, null, null);

            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }

        public void TriggerDecrease(PluginManager pluginManager, string data)
        {
            try
            {
                Action<int> act = HandleSwitch;
                act.BeginInvoke((StepSize*(-1)), null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }

        public delegate int CurrentValueGetter();
    }
}
