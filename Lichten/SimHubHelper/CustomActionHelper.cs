﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// Allows for setting up of Actions that run functions with one parameter.
    /// Ideal for letting users set up custom actions that allows them to set something to that specific value they configured beforehand
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomActionHelper<T>
    {
        public Action<T> Method { get; set; }
        public object[] Param { get; set; }

        public CustomActionHelper() { }

        public CustomActionHelper(Action<T> method, object[] param)
        {
            this.Method = method;
            this.Param = param;
        }

        public void TriggerAction(PluginManager pluginManager, string data)
        {
            try
            {
                //Method.Invoke(Param[0].ToString());
                Method.BeginInvoke((T)Param[0], null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }
    }
}
