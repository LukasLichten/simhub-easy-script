﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// This is a simple DataEventHandler, it takes a simhub property and triggers events based on that property
    /// You need to call the RegisterEvent function, otherwise you can not trigger the event
    /// </summary>
    /// <typeparam name="T">data type of the property</typeparam>
    public class DataEventHelper<T> : IDataEvent
    {
        public string EventName { get; set; }
        public Type Plugin { get; set; }
        public T PreviousValue { get; protected set; }
        public string Property { get; set; }
        protected DataEventHelper() { }

        /// <summary>
        /// When overwriting this function you need to call AddEvent in the pluginManager
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="plugin"></param>
        /// <param name="property"></param>
        protected DataEventHelper(string eventName, Type plugin, string property)
        {
            EventName = eventName;
            Plugin = plugin;
            Property = property;
        }

        public DataEventHelper(string eventName, Type plugin, string property, PluginManager pluginManager) : this(eventName, plugin, property)
        {
            pluginManager.AddEvent(EventName, Plugin);
        }

        /// <summary>
        /// Trigger this from the DataUpdate function
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Update(PluginManager pluginManager)
        {
            object val = pluginManager.GetPropertyValue(Property);

            if (val is T)
            {
                T value = (T)val;

                Evaluate(pluginManager, value);

                PreviousValue = value;
            }
        }

        /// <summary>
        /// Overwrite this function to change the event trigger or to trigger multiple events from the same class.
        /// Per default this event will Trigger on Value change.
        /// 
        /// Value being writen to Previous Value is handled by Update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="value"></param>
        protected virtual void Evaluate(PluginManager pluginManager, T value)
        {
            if ((PreviousValue != null && !PreviousValue.Equals(value)) || (PreviousValue == null && value != null))
            {
                TriggerEvent(EventName, pluginManager);
            }
        }

        protected void TriggerEvent(string name, PluginManager pluginManager)
        {
            try
            {
                pluginManager.TriggerEvent(name, Plugin);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Event " + Plugin.Name + "." + name + ": " + ex.Message);
            }
        }
    }
}
