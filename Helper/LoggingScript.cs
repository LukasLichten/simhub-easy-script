﻿using Lichten.SimHubHelper;
using Newtonsoft.Json;
using SimHub.Plugins.OutputPlugins.Dash.TemplatingCommon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.Helper
{
    public class LoggingScript : Script
    {
        public string FilePath { get; set; }

        [JsonIgnore]
        public NCalcEngineBase NCalcEngine { get; set; }

        [JsonIgnore]
        public ActionHelper ActionHelper { get; set; }

        public void Write()
        {
            if (!Enabled || string.IsNullOrEmpty(FilePath))
                return;


            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
                    throw new IOException("Folder of this File does not exist, so File can't be created");

                object obj = RunNcalc(NCalcEngine);

                string output = "";

                if (obj is string)
                    output = (string)obj;
                else if (obj != null)
                    output = obj.ToString();

                File.AppendAllText(FilePath, output + "\n");
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("EasyScript: Exception thrown when trying to log to file \"" + FilePath + "\": " + ex.Message);
            }
        }
    }
}
