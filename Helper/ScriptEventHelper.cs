﻿using Lichten.SimHubHelper;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.Helper
{
    public class ScriptEventHelper : EventHelper, IDataEvent
    {
        private EventScript Script { get; set; }
        private EasyScript Main { get; set; }

        private bool lastValue = false;

        private int skipCounter = 0;

        private readonly Action Action;

        public ScriptEventHelper(string eventName, EasyScript main, EventScript script) : base(eventName, main.GetType(), main.PluginManager)
        {
            Script = script;   
            Main = main;
            Action = RunScript;
        }

        public void Update(PluginManager pluginManager)
        {
            if (!Script.Enabled)
                return;

            if (skipCounter >= Script.UpdateSkip)
            {
                skipCounter = 0;

                if ((DateTime.Now - Script.LastFailTime).TotalSeconds < Main.Settings.SuspendFailedScriptExecutionSec)
                    return;

                if (Script.ParallelExecution)
                {
                    Action.BeginInvoke(null, null);
                }
                else
                {
                    RunScript();
                }

            }
            else
            {
                skipCounter++;
            }
        }

        private void RunScript()
        {
            object obj = Script.RunNcalc(Main.NCalcEngine);

            //Processing the value
            bool value = false;

            if (obj is bool truth)
                value = truth;
            else if (obj is string txt)
            {
                txt = txt.Trim();

                if (Double.TryParse(txt, out double fCache))
                {
                    value = Math.Round(fCache, 1) == 1.0;
                }
                else if (Boolean.TryParse(txt, out bool bCache))
                {
                    value = bCache;
                }
            }
            else if (obj is double num)
                value = Math.Round(num, 1) == 1.0;
            else if (obj is int inter)
                value = inter == 1;

            //Evaluting If Event Should be raised
            if (value != lastValue)
            {
                switch (Script.EventTrigger)
                {
                    case EventTriggers.Rising:
                        if (value)
                            TriggerEvent(EventName);
                        break;
                    case EventTriggers.Falling:
                        if (lastValue)
                            TriggerEvent(EventName);
                        break;
                    case EventTriggers.Both:
                        TriggerEvent(EventName);
                        break;
                }

                lastValue = value;
            }
        }
    }
}
