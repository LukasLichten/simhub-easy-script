﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.Helper
{
    public class EventScript: Script, IUpdateSetting
    {
        [JsonIgnore]
        public ScriptEventHelper ScriptEventHelper{ get; private set; }

        public EventTriggers EventTrigger { get; set; } = EventTriggers.Rising;

        public bool ParallelExecution { get; set; } = false;
        public int UpdateSkip { get; set; } = 0;

        public void Generate(EasyScript main, string name)
        {
            ScriptEventHelper = new ScriptEventHelper(name, main, this);
            ScriptEventHelper.RegisterEvent();
        }
    }

    public enum EventTriggers
    {
        Rising, Falling, Both
    }
}
