﻿using Newtonsoft.Json;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.Helper
{
    public class PropertyScript : Script, IUpdateSetting
    {
        public bool ParallelExecution { get; set; } = false;
        public int UpdateSkip { get; set; } = 0;

        [JsonIgnore]
        public IProcess Processor { get; private set; }

        public OutputType OutputType
        {
            get => _outputType;
            set
            {
                if (_outputType != value)
                {
                    _outputType = value;
                    SwitchProcessor();
                }
            }
        }

        private OutputType _outputType = OutputType.String;

        private int skipCounter = 0;
        private EasyScript Main { get; set; }
        private Action Action { get; set; }

        public PropertyScript()
        {
            Action = RunScript;
        }

        public void Generate(PluginManager pluginManager, string name, EasyScript type)
        {
            if (Processor == null)
                SwitchProcessor();

            Main = type;

            Processor.Generate(pluginManager, name, type.Propertys.GetType());
        }

        private void SwitchProcessor()
        {
            string name = null;
            if (Processor != null)
                name = Processor.Name;

            switch (OutputType)
            {
                case OutputType.String:
                    Processor = new StringOutput();
                    break;
                case OutputType.Int:
                    Processor = new IntOutput();
                    break;
                case OutputType.Double:
                    Processor = new DoubleOutput();
                    break;
                case OutputType.Boolean:
                    Processor = new BooleanOutput();
                    break;
                case OutputType.TimeSpan:
                    Processor = new TimeSpanOutput();
                    break;
            }

            if (Main != null && name != null)
            {
                Processor.Generate(Main.PluginManager, name, Main.Propertys.GetType());
            }
        }

        public void Update()
        {
            if (!this.Enabled || Processor == null)
                return;

            if (skipCounter >= this.UpdateSkip)
            {
                skipCounter = 0;

                if ((DateTime.Now - this.LastFailTime).TotalSeconds < Main.Settings.SuspendFailedScriptExecutionSec)
                    return;

                if (this.ParallelExecution)
                {
                    Action.BeginInvoke(null, null);
                }
                else
                {
                    RunScript();
                }

            }
            else
            {
                skipCounter++;
            }
        }

        private void RunScript()
        {
            object obj = this.RunNcalc(Main.NCalcEngine);

            Processor.Process(obj);
        }

        private class StringOutput : Processor<string>
        {
            public override void Process(object obj)
            {
                if (Attached == null)
                    return;

                string val = null;

                if (obj is string)
                    val = (string)obj;
                else if (obj != null)
                    val = obj.ToString();

                Attached.Value = val;
            }
        }

        private class IntOutput : Processor<int>
        {
            public override void Process(object obj)
            {
                if (Attached == null)
                    return;

                int val = -1;

                if (obj is int)
                    val = (int)obj;
                else if (obj is long || obj is float || obj is double)
                {
                    double t = Double.Parse(obj.ToString()); //Why .Net, why can I not cast an object into a number type, except if it is that specfic number type? 
                    val = (int)t;
                }
                else if (obj is string txt)
                {
                    txt = txt.Trim();

                    if (Double.TryParse(txt, out double temp))
                        val = (int)temp;
                    else if (Boolean.TryParse(txt, out bool bCache))
                        val = bCache ? 1 : 0;
                }
                else if (obj is bool)
                    val = (bool)obj ? 1 : 0;

                Attached.Value = val;
            }
        }

        private class DoubleOutput : Processor<double>
        {
            public override void Process(object obj)
            {
                if (Attached == null)
                    return;

                double val = Double.NaN;

                if (obj is double)
                    val = (double)obj;
                else if (obj is int || obj is long || obj is float)
                    val = Double.Parse(obj.ToString()); //Same business as with int
                else if (obj is string txt)
                {
                    txt = txt.Trim();

                    if (Double.TryParse(txt, out double temp))
                        val = temp;
                    else if (Boolean.TryParse(txt, out bool bCache))
                        val = bCache ? 1 : 0;
                }
                else if (obj is bool)
                    val = (bool)obj ? 1 : 0;

                Attached.Value = val;
            }
        }

        private class BooleanOutput : Processor<bool>
        {
            public override void Process(object obj)
            {
                if (Attached == null)
                    return;

                bool val = false;

                if (obj is bool)
                    val = (bool)obj;
                else if (obj is string txt)
                {
                    txt = txt.Trim();

                    if (Boolean.TryParse(txt, out bool bCache))
                        val = bCache;
                    else if (Double.TryParse(txt, out double temp))
                        val = Math.Round(temp, 1) == 1.0;
                }
                else if (obj is int || obj is long || obj is float || obj is double)
                {
                    double temp = Double.Parse(obj.ToString()); //Same issue as with int
                    val = Math.Round(temp, 1) == 1.0;
                }
                    

                Attached.Value = val;
            }
        }

        private class TimeSpanOutput : Processor<TimeSpan>
        {
            public override void Process(object obj)
            {
                if (Attached == null)
                    return;

                TimeSpan val = TimeSpan.Zero;

                if (obj is TimeSpan)
                    val = (TimeSpan)obj;
                else if (obj is string txt)
                {
                    txt = txt.Trim();

                    if (TimeSpan.TryParse(txt, out TimeSpan tCache))
                        val = tCache;
                    else if (Double.TryParse(txt, out double fCache))
                        val = TimeSpan.FromSeconds(fCache);
                }
                else if (obj is int || obj is long || obj is float || obj is double)
                {
                    double temp = Double.Parse(obj.ToString()); //Same issue as with int
                    val = TimeSpan.FromSeconds(temp);
                }

                Attached.Value = val;
            }
        }
    }

    public enum OutputType
    {
        String, Int, Double, Boolean, TimeSpan
    }

    public interface IProcess
    {
        string Name { get; }

        void Generate(PluginManager pluginManager, string name, Type type);

        void Process(object obj);
    }

    public abstract class Processor<T> : IProcess
    {
        public string Name { get; protected set; }
        protected AttachedProperty<T> Attached { get; set; }

        public virtual void Generate(PluginManager pluginManager, string name, Type type)
        {
            Name = name;

            Attached = new AttachedProperty<T>();
            pluginManager.AttachProperty(name, type, Attached);
        }

        public abstract void Process(object obj);
    }
}
