﻿using Newtonsoft.Json;
using SimHub.Plugins.OutputPlugins.Dash.GLCDTemplating;
using SimHub.Plugins.OutputPlugins.Dash.TemplatingCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Script.Helper
{
    public class Script
    {
        [JsonIgnore]
        private ExpressionValue ExpressionValue { get; set; } = new ExpressionValue();

        private DateTime _lastFailTime = DateTime.MinValue;

        [JsonIgnore]
        public DateTime LastFailTime {
            get => _lastFailTime;
        }

        public bool Enabled { get; set; } = true;

        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;
        }

        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                if (_expression != value)
                {
                    _expression = value;
                    ExpressionValue.Expression = _expression;
                }
            }
        }

        private string _expression = "";

        public Interpreter Interpreter
        {
            get
            {
                return _interpreter;
            }
            set
            {
                if (_interpreter != value)
                {
                    _interpreter = value;
                    ExpressionValue.Interpreter = _interpreter;
                }
            }
        }

        private Interpreter _interpreter = Interpreter.NCalc;

        public string Format { get; set; } = null;

        public object RunNcalc(NCalcEngineBase NCalcEngine)
        {
            if (!string.IsNullOrEmpty(ExpressionValue.Expression) && Enabled)
            {
                object obj;
                try
                {
                    ExpressionValue.Invalid = false;
                    obj = NCalcEngine.ParseValue(ExpressionValue);
                    if (ExpressionValue.Invalid)
                    {
                        _lastFailTime = DateTime.Now;
                        return null;
                    }
                }
                catch
                {
                    _lastFailTime = DateTime.Now;
                    return null;
                }

                if (!string.IsNullOrEmpty(this.Format))
                {
                    try
                    {
                        obj = string.Format("{0:" + Format + "}", obj ?? "");
                    }
                    catch
                    {
                        _lastFailTime = DateTime.Now;
                        obj = "Formating Error";
                    }
                }

                return obj;
            }

            _lastFailTime = DateTime.Now;
            return null;
        }
    }
}
