# EasyScript
Create Custom Events, Properties and do Logging, configured through the UI, using SimHub's built in NCalc/JScript engine.  
  
SimHub is required for this plugin to work: [Here](https://www.simhubdash.com/)  
  
You can Download EasyScript Here: [Releases](https://gitlab.com/LukasLichten/simhub-easy-script/-/releases) [Racedepartment](https://www.racedepartment.com/downloads/simhub-easyscript.53612/)  
  
## Current Features
- Events
   - Write a NCalc/JScript expresion as condition
   - Set the Update Rate for how often the condition should be checked
   - Set if the event should trigger when the condition goes from false to true (Rising), true to false (Falling), or Both
- Propertys
   - Write the Output of the property as a NCalc/JScript function
   - Standard Formating of the Output is available
   - Set the Update Rate for how often the property should be updated
   - And or run the update for the property in parallel (spawning the async task costs time too, so only worth it on large JScript functions)
   - Choice from Boolean, Int, Double, TimeSpan or String as DataType
- Logging
   - Create an Action (which you can then attach to an Event or Input in "Controls and events") that logs to a file
      - Multiple Actions can log to the same file
      - Files will be created if they don't exist (as long as the folders exist)
   - The Output is what your NCalc/JScript function return, meaning the output is dynamic
   - Uncheck the Checkbox in the First Colum to temporally disable this action
- Exporting And Importing of your creations
  
## Images
![loggs](https://www.racedepartment.com/attachments/2022-08-19-1-png.592804/ "Logging Example")  
  
![props](https://www.racedepartment.com/attachments/2022-08-19-2-png.592805/ "Properties Example")  
  
![editing](https://www.racedepartment.com/attachments/2022-08-19-3-png.592806/ "Editing Properties Example")  
  
## Installation
1. Make sure to have [SimHub](https://www.simhubdash.com/) installed and set up
2. Download [EasyScript](https://gitlab.com/LukasLichten/simhub-easy-script/-/releases)
3. Extract the SimHub-EasyScript.dll and place it into the SimHub root folder
4. Upon starting SimHub it will prompt you to enable the Plugin (you may select "Show In left main menu")
5. You will find it now in the Left Menu (as it's own tile, or in the 3 dots menu "Additional Plugins")

Usage Advice:  
If you want to exit the coding window without saving changes, then you can select None instead of Computed Value (compared to Dashboard mapping this won't delete your code).  
Or you press ESC
  
## Build Instructions
The project is set up for SimHub to be installed on the same drive, with this repo in a git folder that is in the root (example: x:\git\SimHub-Easy Script/README.md). Given this, all references should load, you should be able to build an debug the project through Visial Studio.
